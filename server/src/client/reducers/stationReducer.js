import { CREATE_STATION } from "../actions";

export default (state = [], action) => {
  switch (action.type) {
    case CREATE_STATION:
      return {
        ...state,
        code: action.payload.data
      };
    default:
      return state;
  }
};
