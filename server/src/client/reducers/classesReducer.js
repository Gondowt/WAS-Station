import { FETCH_MY_CLASSES, FETCH_A_CLASSE } from '../actions';

export default (state = [], action) => {
  switch (action.type) {
    case FETCH_MY_CLASSES:
      return {
        ...state,
        list: action.payload.data
      };
    case FETCH_A_CLASSE:
      return {
        ...state,
        current: action.payload.data
      };
    default:
      return state;
  }
};
