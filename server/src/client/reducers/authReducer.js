import { FETCH_CURRENT_USER, AUTHENTICATE_USER, ERROR_AUTHENTICATE_USER } from '../actions';

export default function (state = null, action) {
  switch (action.type) {
    case FETCH_CURRENT_USER:
      return action.payload.data || false;
    case AUTHENTICATE_USER:
      return action.payload.data || false;
    case ERROR_AUTHENTICATE_USER:
      return false;
    default:
      return state;
  }
}
