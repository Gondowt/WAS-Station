import React, { Component } from "react";
import { Helmet } from "react-helmet";
import { Link } from "react-router-dom";
import { Button, Row, Col } from "antd";
import { connect } from "react-redux";

class MyStationPage extends Component {
  head = () => (
    <Helmet>
      <title>WAS - Ma station</title>
      <meta property="og:title" content="WAS - Login" />
    </Helmet>
  );

  render() {
    return (
      <div>
        <Row type="flex" justify="end">
          {this.props.auth.role === "teacher" && (
            <Col>
              <Link to="/station/add">
                <Button type="primary">Ajouter une station</Button>
              </Link>
            </Col>
          )}
        </Row>
        <Row type="flex" justify="center">
          <Col>
            <h1>Mes Station</h1>
          </Col>
        </Row>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return { auth: state.auth };
}

export default {
  component: connect(mapStateToProps)(MyStationPage)
};
