import React, { Component } from 'react';
import axios from 'axios';
import { Table, Row, Col, Button, Icon } from 'antd';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';
import { fetchAClasse } from '../actions';

const columns = [{
  title: 'Nom',
  dataIndex: 'name',
  key: 'name',
}];

class Classe extends Component {

  componentWillMount() {
    this.props.fetchAClasse(this.props.match.params.id)
  }

  render() {
    return (
      <div>
        {this.props.classes.current ?
          <div className="center-align">
            <Row type="flex" justify="space-between">
              <Button type="primary" onClick={() => this.props.history.goBack()}><Icon type="left" />Retour</Button>
              <h1>Mes classes {this.props.classes.current.name}</h1>
              <Link to="/classes/addStudent"><Button type="primary">Ajouter un élève</Button></Link>
            </Row>
            <Table dataSource={this.props.classes.current.students} columns={columns}/>
          </div>
        :
        <div className="center-align">
          <h1>Ma classe</h1>
        </div>
        }
      </div>
    );
  }
};

function mapStateToProps(state) {
  return state;
}

export default {
  component: connect(mapStateToProps, { fetchAClasse })(Classe)
};
