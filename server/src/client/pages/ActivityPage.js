import React, { Component } from "react";
import axios from "axios";
import { Button, Row, Col, Icon } from "antd";
import { connect } from "react-redux";
import { fetchActivity, completeActivity, removeCurrentActivity } from "../actions";
import DragAndDrop from "../components/DragAndDrop";
import TrucARelier from "../components/TrucARelier";
import Quizz from "../components/Quizz";

class ActivityPage extends Component {
  constructor(props) {
    super(props);

    this.state = {
      canValidate: false,
      sendAnswer: null,
      results: null,
      next: false,
      waitingValue: false,
      responses: []
    };

    if (Object.keys(props.activity).length < 1) props.history.push("/themes");
  }

  componentWillUnmount() {
    this.props.removeCurrentActivity();
  }

  getAnswer = (callback, result) => {
    switch (this.props.activity.type) {
      case "dragDrop":
        this.setState({
          canValidate: !(result.filter(item => item.idDrag === -1).length > 0),
          sendAnswer: callback,
          results: result
        });
        break;
      case "trucARelier":
        this.setState({
          canValidate: Object.keys(result).length === this.props.activity.data.endZones.length,
          results: result
        });
        break;
      case "quizz":
        this.setState({
          waitingValue: false
        });
        if (result !== undefined) {
          axios
            .post(`/api/activity/${this.props.activity.uuid}/check`, {
              type: "quizz",
              data: result
            })
            .then(response => {
              callback(response.data.results);
              this.setState({
                next: response.data.isCompleted
              });
              response.data.isCompleted && this.props.completeActivity(this.props.activity.uuid);
            })
            .catch(response => console.log(response));
        }
        break;
      default:
        break;
    }
  };

  checkAnswer = () => {
    switch (this.props.activity.type) {
      case "dragDrop":
        axios
          .post(`/api/activity/${this.props.activity.uuid}/check`, {
            type: "dragDrop",
            data: this.state.results
          })
          .then(response => {
            this.state.sendAnswer(response.data.results.map(result => ({ id_drop_zone: result.idDropZone, result: result.result })));
            this.setState({
              next: response.data.isCompleted
            });
            response.data.isCompleted && this.props.completeActivity(this.props.activity.uuid);
          })
          .catch(response => console.log(response));
        break;
      case "trucARelier":
        axios
          .post(`/api/activity/${this.props.activity.uuid}/check`, {
            type: "trucARelier",
            data: this.state.results
          })
          .then(response => {
            this.setState({
              responses: response.data.results
            });
            this.setState({
              next: response.data.isCompleted
            });
            response.data.isCompleted && this.props.completeActivity(this.props.activity.uuid);
          })
          .catch(response => console.log(response));
        break;
      case "quizz":
        this.setState({
          waitingValue: true
        });
        break;
      default:
        break;
    }
  };

  cleanResponse = () => {
    this.setState({
      responses: []
    });
  };

  selectActivity = () => {
    switch (this.props.activity.type) {
      case "dragDrop":
        return <DragAndDrop data={this.props.activity.data} validate={this.getAnswer} />;
      case "quizz":
        return <Quizz data={this.props.activity.data} waitingValue={this.state.waitingValue} sendAnswer={this.getAnswer} />;
      case "trucARelier":
        return <TrucARelier data={this.props.activity.data} validate={this.getAnswer} responses={this.state.responses} cleanResponse={this.cleanResponse} />;
      default:
        break;
    }
  };

  render() {
    return (
      <div>
        <Button type="primary" onClick={() => this.props.history.goBack()}>
          <Icon type="left" />Retour
        </Button>
        <h1 style={{ textAlign: "center" }}>{this.props.activity.name}</h1>
        <div className="activity">{this.selectActivity()}</div>
        <Row className="validateActivity" type="flex" justify="space-between" style={{ marginTop: 20 }}>
          <Col>
            {this.props.activity.type === "quizz" ? (
              <Button type="primary" onClick={this.checkAnswer}>
                Valider
              </Button>
            ) : (
              <Button type="primary" onClick={this.checkAnswer} disabled={!this.state.canValidate}>
                Valider
              </Button>
            )}
          </Col>
          <Col>
            <Button type="primary" onClick={() => this.props.history.goBack()} disabled={!this.state.next}>
              Activité Suivante
            </Button>
          </Col>
        </Row>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    activity: state.activity
  };
}

// export loadData and component for Routes functions
export default {
  component: connect(mapStateToProps, { fetchActivity, completeActivity, removeCurrentActivity })(ActivityPage)
};
