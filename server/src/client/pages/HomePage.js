import React, { Component } from 'react';
import { Redirect } from 'react-router'
import axios from 'axios';
import CardLineChart from '../components/CardLineChart';
import UserInfoCard from '../components/UserInfoCard';
import { connect } from 'react-redux';
import { Row, Col } from 'antd';
import { Helmet } from 'react-helmet';

class Home extends Component {

  componentDidMount() {
  }

  head() {
    return (
      <Helmet>
        <title>Home</title>
      </Helmet>
    );
   }

  render() {
    return (
      <div className="center-align">
      {this.head()}
        <Row type="flex" justify="space-around" gutter={20}>
          <Col md={{ span: 8, order: 1 }} sm={{ span: 24, order: 2 }} xs={{ span: 24, order: 2 }}>
            <CardLineChart
              title={'Température'}
              id={'Température'}
              labelY={'Degré'}
              labelX={'Date'}
              dataPath={'data/data.csv'}
            />
          </Col>
          <Col md={{ span: 8, order: 2 }} sm={{ span: 24, order: 1 }} sm={{ span: 24, order: 1 }}>
            <UserInfoCard
              name={this.props.auth.name}
              image={"img/avatar.png"}
            />
          </Col>
          <Col md={{ span: 8, order: 3 }} sm={{ span: 24, order: 2 }} xs={{ span: 24, order: 2 }}>
            <CardLineChart
              title={'UV'}
              id={'UV'}
              labelY={'Degré'}
              labelX={'Date'}
              dataPath={'data/data.1.csv'}
            />
          </Col>
        </Row>
        <Row type="flex" justify="space-around" gutter={20}>
          <Col md={{ span: 8, order: 1 }} sm={{ span: 24, order: 2 }} xs={{ span: 24, order: 2 }}>
            <CardLineChart
              title={'Température'}
              id={'Température2'}
              labelY={'Degré'}
              labelX={'Date'}
              dataPath={'data/data.csv'}
            />
          </Col>
          <Col md={{ span: 8, order: 1 }} sm={{ span: 24, order: 2 }} xs={{ span: 24, order: 2 }}>
            <CardLineChart
              title={'Température'}
              id={'Température3'}
              labelY={'Degré'}
              labelX={'Date'}
              dataPath={'data/data.csv'}
            />
          </Col>
          <Col md={{ span: 8, order: 3 }} sm={{ span: 24, order: 2 }} xs={{ span: 24, order: 2 }}>
            <CardLineChart
              title={'UV'}
              id={'UV2'}
              labelY={'Degré'}
              labelX={'Date'}
              dataPath={'data/data.1.csv'}
            />
          </Col>
        </Row>
      </div>
    );
  }
};

function mapStateToProps(state) {
  return state;
}

export default {
  component: connect(mapStateToProps)(Home)
};
