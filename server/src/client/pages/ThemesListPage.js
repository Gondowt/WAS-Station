import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Button, Row, Col } from 'antd';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import ThemeCard from '../components/ThemeCard';
import { fetchThemes, fetchCurrentTheme, fetchMyClasses } from '../actions';

class ThemeList extends Component {
  componentDidMount() {
    this.props.fetchThemes();
  }

  navigate = (id) => {
    this.props.auth.role === 'teacher'
      ? this.props.fetchCurrentTheme(id).then(() => {
        this.props.fetchMyClasses().then(() => this.props.history.push(`/theme/${id}/teacher`));
      })
      : this.props.fetchCurrentTheme(id).then(() => this.props.history.push(`/theme/${id}`));
  };

  renderThemeList = () => this.props.themes.list.map(item => (
    <Col key={item.uuid} md={{ span: 8 }} sm={{ span: 24 }} xs={{ span: 24 }}>
      <ThemeCard teacher={this.props.auth.role === 'teacher'} theme={item} navigate={this.navigate} />
    </Col>
  ));

  head() {
    return (
      <Helmet>
        <title>Liste des thèmes</title>
      </Helmet>
    );
  }

  render() {
    return (
      <div>
        {this.head()}
          <Row type="flex" justify="space-between">
            <Col>
              <h1>Liste des thèmes</h1>
            </Col>
            {this.props.auth.role === 'teacher' && (
            <Col>
              <Link to="/themes/add">
                <Button type="primary">Créer un nouveau thème</Button>
              </Link>
            </Col>
          )}
          </Row>
            <Row gutter={20}>{this.renderThemeList()}</Row>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return state;
}

// export loadData and component for Routes functions
export default {
  component: connect(mapStateToProps, { fetchThemes, fetchCurrentTheme, fetchMyClasses })(ThemeList),
};
