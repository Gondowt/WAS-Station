import React, { Component } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { Form, Icon, Input, Button, Row, Col } from 'antd';

const FormItem = Form.Item;

class AddThemePage extends Component {

  handleSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        axios.post('/api/themes', values)
          .then((result) => {
            this.props.history.push('/themes');
          })
          .catch((err) => console.log(err));
      }
    });
  }

  render() {
    const { getFieldDecorator } = this.props.form;
    return (
      <Form onSubmit={this.handleSubmit} className="login-form">
        <Row type="flex" justify="space-between">
          <Button type="primary" onClick={() => this.props.history.goBack()}><Icon type="left" />Retour</Button>
        </Row>
        <Row>
          <h1 style={{ textAlign: 'center' }}>Créer un nouveau thème</h1>
        </Row>
        <FormItem label="Nom">
          {getFieldDecorator('name', {
            rules: [{ required: true, message: 'Vous devez renseigner le nom du thème' }],
          })(
            <Input placeholder="Nom du theme" />
          )}
        </FormItem>
        <FormItem>
          <Button type="primary" htmlType="submit" className="login-form-button">
            Créer
          </Button>
        </FormItem>
      </Form>
    );
  }
};

const WrappedAddThemePage = Form.create()(AddThemePage);

function mapStateToProps(state) {
  return state;
}

export default {
  component: connect(mapStateToProps)(WrappedAddThemePage)
};
