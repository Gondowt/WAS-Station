import React, { Component } from 'react';
import { withRouter } from 'react-router-dom'
import { Button, Row, Col, Icon, Tag, Divider, notification, Tooltip, Input, Modal } from 'antd';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { Helmet } from 'react-helmet';
import { Redirect } from 'react-router-dom';
import { fetchCurrentTheme, fetchActivity, addThemeToClasse, removeThemeToClasse, updateThemeTitle } from '../actions';
import CheckableTag from '../components/CheckableTag';



class ThemePage extends Component {
  constructor(props) {
    super(props)

    this.state = {
      titleIsEditable: false,
      theme: props.themes.current ? props.themes.current : '',
    }

    !props.themes.current && props.history.push('/themes')
  }

  navigateToActivity = (id) => {
    this.props.fetchActivity(id).then(() => this.props.history.push(`/activity/${id}`))
  }

  renderActivitiesList = () => {
    return this.props.themes.current.activities.map((item) => {
      return (
        <Col
          key={item.uuid}
          md={{ span: 24 }} sm={{ span: 24 }} xs={{ span: 24 }}
          style={{ marginBottom: 20, padding: 20, backgroundColor: '#fafafa' }}
          onClick={() => this.navigateToActivity(item.uuid)}
        >
          <Row type="flex" justify="space-between">
            <Col>
              <h2>{item.name}</h2>
            </Col>
            <Col>
              <Button disabled={item.done} type="primary">Consulter</Button>
            </Col>
          </Row>
        </Col>
      )
    })
  }

  handleClassesChange = (id, value, name) => {
    value
      ?
        this.props.addThemeToClasse(id, this.props.themes.current.uuid).then(
          notification["success"]({
            message: `Ce thème est maintenant visible par la classe de ${name}`,
          })
        )
      :
        this.props.removeThemeToClasse(id, this.props.themes.current.uuid).then(
          notification["success"]({
            message: `Ce thème n'est plus visible par la classe de ${name}`,
          })
        )
  }

  renderClassesList = () => {
    return this.props.classes.list.map((item) => {
      return (
        <CheckableTag
          key={item.uuid}
          id={item.uuid}
          initialValue={this.props.themes.current.listOfClasses.findIndex(element => element.uuid === item.uuid) > -1}
          handleChange={this.handleClassesChange}
        >
          {item.name}
        </CheckableTag>
      )
    })
  }

  head() {
    return (
      <Helmet>
        <title>{this.props.themes.current.name}</title>
      </Helmet>
    );
  }

  toggleEditTheme = () => {
    this.setState({ titleIsEditable: true })
  }

  handleChange = (e) => { 
    this.setState({
      theme: {
        ...this.state.theme,
        name: e.target.value
      }
    })
  }

  changeTitle = () => {
    this.props.updateThemeTitle(this.state.theme.name, this.state.theme.uuid).then(() => {
      this.setState({ titleIsEditable: false })
      notification["success"]({
        message: `Le titre de ce thème à bien été édité.`,
      })
    })
  }

  deleteTheme = () => {
    Modal.confirm({
      title: 'Confirmation',
      content: 'Ce thème va être définitivement supprimé de vos thèmes. Êtes-vous sûr ?',
      okText: 'Oui',
      okType: 'danger',
      cancelText: 'Non',
      onOk() {
        console.log('Oui');
      },
      onCancel() {
        console.log('Non');
      },
    });
  }

  render() {
    return this.props.themes.current &&
      <div>
        {this.head()}
        <Row type="flex" justify="space-between">
          <Col>
            <Button type="primary" onClick={() => this.props.history.goBack()}>
              <Icon type="left" />
              Retour
            </Button>
          </Col>
          <Col>
            <Button icon="close" type="danger" onClick={this.deleteTheme}>
              Supprimer ce thème
            </Button>
          </Col>
        </Row>
        <Row type="flex" justify="center" align="middle" gutter={8}>
          <Col>
            {this.state.titleIsEditable
              ?
                <Input
                  id="name"
                  placeholder="Titre du thème"
                  value={this.state.theme.name}
                  onChange={this.handleChange}
                />
              :
                <h1 style={{ textAlign: 'center', margin: 0 }}>{this.props.themes.current.name}</h1>
            }
          </Col>
          <Col>
            {this.state.titleIsEditable
                ?
                  <Button icon="check" onClick={this.changeTitle}></Button>
                :
                  <Tooltip title="Éditer le titre de ce thème">
                    <Icon onClick={this.toggleEditTheme} type="edit" style={{ fontSize: 20, color: "#8f8f8f" }}/>
                  </Tooltip>
              }
            
          </Col>
        </Row>
        <Divider>Liste des classes</Divider>
        <Row>
          <p>Voici la liste des classes pouvant accéder à ce thème.</p>
        </Row>
        <Row>
          {this.renderClassesList()}
        </Row>
        <Divider>Liste des activités</Divider>
        <Row type="flex" justify="space-around">
          {this.renderActivitiesList()}
          <Button icon="plus" type="dashed" style={{ width: '100%', height: 80 }}>Ajouter une nouvelle activité...</Button>
        </Row>
      </div>
  }
}

function mapStateToProps(state) {
  return state;
}

// export loadData and component for Routes functions
export default {
  component: connect(mapStateToProps, { fetchCurrentTheme, fetchActivity, addThemeToClasse, removeThemeToClasse, updateThemeTitle })(ThemePage)
};
