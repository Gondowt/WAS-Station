import React, { Component } from "react";
import axios from "axios";
import { connect } from "react-redux";
import { Form, Icon, Input, Button, Row } from "antd";

const FormItem = Form.Item;

class AddClasseForm extends Component {
  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        axios
          .post("/api/classes", values)
          .then(() => {
            this.props.history.push("/classes");
          })
          .catch(() => console.log(err));
      }
    });
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    return (
      <Form onSubmit={this.handleSubmit} className="login-form">
        <Row type="flex" justify="space-between">
          <Button type="primary" onClick={() => this.props.history.goBack()}>
            <Icon type="left" />Retour
          </Button>
          <h1>Créer une nouvelle classe</h1>
        </Row>
        <FormItem label="Nom">
          {getFieldDecorator("name", {
            rules: [{ required: true, message: "Vous devez renseigner le nom de la classe" }]
          })(<Input placeholder="Nom de la classe" />)}
        </FormItem>
        <FormItem>
          <Button type="primary" htmlType="submit" className="login-form-button">
            Créer
          </Button>
        </FormItem>
      </Form>
    );
  }
}

const WrappedAddClasseForm = Form.create()(AddClasseForm);

function mapStateToProps(state) {
  return state;
}

export default {
  component: connect(mapStateToProps)(WrappedAddClasseForm)
};
