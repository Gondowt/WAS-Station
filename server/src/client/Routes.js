import React from "react";
import App from "./App";
import HomePage from "./pages/HomePage";
import ClassesPage from "./pages/ClassesPage";
import ClassePage from "./pages/ClassePage";
import ThemesListPage from "./pages/ThemesListPage";
import ThemePage from "./pages/ThemePage";
import ThemeTeacherPage from "./pages/ThemeTeacherPage";
import AddThemePage from "./pages/AddThemePage";
import EditThemePage from "./pages/EditThemePage";
import NotFoundPage from "./pages/NotFoundPage";
import AdminsListPage from "./pages/AdminsListPage";
import LoginPage from "./pages/LoginPage";
import AddClassePage from "./pages/AddClassePage";
import AddStudentPage from "./pages/AddStudentPage";
import ActivityPage from "./pages/ActivityPage";
import MyStationPage from "./pages/MyStationPage";
import AddStationPage from "./pages/AddStationPage";

export default [
  {
    ...LoginPage,
    path: "/login",
    exact: true
  },
  {
    ...App,
    routes: [
      {
        ...HomePage,
        path: "/",
        exact: true
      },
      {
        ...ClassesPage,
        path: "/classes",
        exact: true
      },
      {
        ...ClassePage,
        path: "/classe/:id"
      },
      {
        ...AddClassePage,
        path: "/classes/add"
      },
      {
        ...AddStudentPage,
        path: "/classes/addStudent"
      },
      {
        ...AdminsListPage,
        path: "/admins"
      },
      {
        ...AddThemePage,
        path: "/themes/add"
      },
      {
        ...ThemesListPage,
        path: "/themes"
      },
      {
        ...EditThemePage,
        path: "/theme/edit/:id"
      },
      {
        ...ThemeTeacherPage,
        path: "/theme/:id/teacher"
      },
      {
        ...ThemePage,
        path: "/theme/:id"
      },
      {
        ...ActivityPage,
        path: "/activity/:id"
      },
      {
        ...AddStationPage,
        path: "/station/add"
      },
      {
        ...MyStationPage,
        path: "/station/"
      },
      {
        ...NotFoundPage
      }
    ]
  }
];
