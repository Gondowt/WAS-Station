import React, { Component } from "react";
import * as d3 from "d3";

class DragAndDrop extends Component {
  constructor(props) {
    super(props);

    function shuffle(a) {
      for (let i = a.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [a[i], a[j]] = [a[j], a[i]];
      }
      return a;
    }

    const formatedDragElements = props.data.dragElements.map(item => ({
      ...item,
      dropZone: -1
    }));

    const dragElementsShuffle = shuffle(formatedDragElements);

    const dropZones = props.data.dropzones.map(item => ({
      ...item,
      occupied: -1
    }));

    this.state = {
      width: props.data.width,
      height: props.data.height,
      dragElements: dragElementsShuffle,
      dropZones,
      padding: 20
    };
  }

  componentDidMount() {
    const dropZone = d3.select(this.refs.dropZone);
    const background = d3.select(this.refs.background);

    this.departZone(() => {
      background
        .append("rect")
        .attr("x", 0)
        .attr("y", 0)
        .attr("width", this.state.width)
        .attr("height", this.state.tailleDropZone)
        .style("fill", "rgb(239, 239, 239)");

      dropZone
        .selectAll("image")
        .data(this.state.dragElements)
        .enter()
        .append("image")
        .attr("xlink:href", d => d.img)
        .attr("x", d => d.x)
        .attr("y", d => d.y)
        .attr("id", d => `i${d.id}`)
        .attr("width", d => d.width)
        .attr("height", d => d.height)
        .call(
          d3
            .drag()
            .on("start", (d, i) => this.startDrag(d, i))
            .on("drag", (d, i) => this.drag(d, i))
            .on("end", (d, i) => this.endDrag(d, i))
        );

      dropZone
        .selectAll("rect")
        .enter()
        .data(this.state.dropZones)
        .enter()
        .append("rect")
        .attr("x", d => d.x)
        .attr("y", d => d.y)
        .attr("id", d => `i${d.id}`)
        .attr("width", d => d.width)
        .attr("height", d => d.height)
        .attr("class", "dropZone");

      background
        .append("g")
        .attr("transform", `translate(0,${this.state.tailleDropZone + 20})`)
        .append("image")
        .attr("xlink:href", this.props.data.img)
        .attr("width", this.props.data.width)
        .attr("height", this.props.data.height);

      this.resizeOrigin();
    });
  }

  getAnswer = results => {
    results.forEach(result => {
      d3.select(`#i${result.id_drop_zone}`).attr("class", result.result ? "dragAndDropRight" : "dragAndDropWrong");
    });
  };

  getIndexCollectionById = (collection, id) => collection.reduce((acc, v, ind) => (v.id === id ? ind : acc), -1);

  distance = (x1, x2, y1, y2) => Math.sqrt((x1 - x2) ** 2 + (y1 - y2) ** 2);

  distanceMin = (x, y) => {
    const dropZonesDisponible = this.state.dropZones.filter(dropZone => this.distance(x, dropZone.x, y, dropZone.y) < 150);

    if (dropZonesDisponible.length > 0) {
      return dropZonesDisponible.reduce(
        (acc, v) =>
          this.distance(x, acc.x, y, acc.y) < this.distance(x, v.x, y, v.y)
            ? acc
            : {
                x: v.x,
                y: v.y,
                width: v.width,
                height: v.height,
                id: v.id,
                occupied: v.occupied
              }
      );
    }
    return -1;
  };

  endDrag = (d, i) => {
    const dropZonesToGo = this.distanceMin(d.x, d.y, i);
    const dropZones_modif = this.state.dropZones.slice();
    const dragElements_modif = this.state.dragElements.slice();

    d3.select(`#i${this.state.dragElements[i].id}`).classed("active", false);

    if (dropZonesToGo.occupied != -1 && dropZonesToGo != -1) {
      const indexdragElement = this.getIndexCollectionById(this.state.dragElements, dropZonesToGo.occupied);
      d3
        .select(`#i${dropZonesToGo.occupied}`)
        .attr("x", (dragElements_modif[indexdragElement].x = this.state.dragElements[indexdragElement].x_start))
        .attr("y", (dragElements_modif[indexdragElement].y = this.state.dragElements[indexdragElement].y_start));
      dragElements_modif[indexdragElement].dropZone = -1;
    }

    if (dropZonesToGo == -1) {
      dragElements_modif[i].dropZone = -1;
      d3
        .select(`#i${this.state.dragElements[i].id}`)
        .attr("x", (d.x = this.state.dragElements[i].x_start))
        .attr("y", (d.y = this.state.dragElements[i].y_start));
    } else {
      dragElements_modif[i].dropZone = dropZonesToGo.id;
      d3
        .select(`#i${this.state.dragElements[i].id}`)
        .attr("x", (d.x = dropZonesToGo.x + dropZonesToGo.width / 2 - d.width / 2))
        .attr("y", (d.y = dropZonesToGo.y + dropZonesToGo.height / 2 - d.height / 2));
    }

    dropZones_modif.forEach(dropZone => {
      dropZone.occupied = -1;
    });

    dragElements_modif.filter(dragElement => dragElement.dropZone != -1).forEach(dragElement => {
      dropZones_modif[this.getIndexCollectionById(dropZones_modif, dragElement.dropZone)].occupied = dragElement.id;
    });

    this.setState({
      dropZones: dropZones_modif,
      dragElements: dragElements_modif
    });

    this.props.validate(this.getAnswer, this.state.dropZones.map(dropZone => ({ idDrop: dropZone.id, idDrag: dropZone.occupied })));
  };

  drag = (d, i) => {
    d3
      .select(`#i${this.state.dragElements[i].id}`)
      .attr("x", (d.x = d3.event.x))
      .attr("y", (d.y = d3.event.y));
  };

  startDrag = (d, i) => {
    d3
      .select(`#i${this.state.dragElements[i].id}`)
      .raise()
      .classed("active", true);
  };

  resizeOrigin = () => {
    const svg = d3.select(document.getElementsByTagName("svg")[0]);
    const parent = d3.select(svg.node().parentNode);
    const { width } = this.state;
    const height = this.state.height + this.state.tailleDropZone + this.state.padding;
    const aspect = width / height;

    d3.select(window).on("resize", resize);

    svg
      .attr("viewBox", `0 0 ${width} ${height}`)
      .attr("preserveAspectRatio", "xMinYMid")
      .call(resize);

    function resize() {
      const targetWidth = parseInt(parent.style("width"));
      svg.attr("width", targetWidth);
      svg.attr("height", Math.round(targetWidth / aspect));
    }
  };

  departZone = callback => {
    const _drag_elements = this.state.dragElements.map(element => Object.assign({}, element));

    let max = 0;
    let x = 0;
    let y = 0;

    _drag_elements.forEach(element => {
      if (this.state.width < x + element.width) {
        x = 5;
        y = y + max + 10;
        max = 0;
      }
      element.x_start = x;
      element.x = x;
      element.y_start = y;
      element.y = y;

      x += element.width + 10;

      max = max < element.height ? element.height : max;
    });

    const _tailleDropZone = y + max + this.state.padding;

    const _drop_zone = this.state.dropZones.map(element => Object.assign({}, element));
    _drop_zone.forEach(element => {
      element.y = element.y + _tailleDropZone + this.state.padding;
    });

    this.setState(
      {
        dragElements: _drag_elements,
        tailleDropZone: _tailleDropZone,
        dropZones: _drop_zone
      },
      callback
    );
  };

  render() {
    return (
      <svg>
        <g ref="anchor">
          <g ref="background" />
          <g ref="dropZone" />
        </g>
      </svg>
    );
  }
}

export default DragAndDrop;
