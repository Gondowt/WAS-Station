import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Avatar, Row, Col } from 'antd';
import * as d3 from "d3";
import RadialProgressChart from './RadialProgressChart';

class UserInfoCard extends Component {
  constructor(props) {
    super(props);

    this.state = {
      x: d3.scaleTime().rangeRound([0, 300]),
      y: d3.scaleLinear().rangeRound([300, 0])
    }
  }

  render() {
    return (
      <div style={{ marginBottom: 20, backgroundColor: '#fafafa', paddingTop: 20, paddingBottom: 20 }}>
        <Row type="flex" justify="center">
          <Col>
            <Avatar
              src={this.props.image}
              size="large"
              style={{ height: 100, width: 100, borderRadius: 50 }}
            />
          </Col>
        </Row>
        <h1 style={{ textAlign: 'center', marginTop: 20, marginBottom: 20 }}>{this.props.name}</h1>
        <hr style={{ width: '80%', margin: 'auto' }} />
        <h2 style={{ textAlign: 'center' }}> Level 12 </h2>
        <RadialProgressChart
          maxValue={120}
          currentValue={90}
          id={'progressChart'}
        />
      </div>
    );
  }
};

export default UserInfoCard;
