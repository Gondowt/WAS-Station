import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Row, Col, Button } from 'antd';
import RadialProgressChart from '../components/RadialProgressChart';

class ThemeCard extends Component {
  render() {
    return (
      <div style={{ marginBottom: 20, backgroundColor: '#fafafa', paddingTop: 20, paddingBottom: 20 }}>
        <Row type="flex" justify="center">
          <Col>
            <h1 style={{ textAlign: 'center' }}>{this.props.theme.name}</h1>
          </Col>
        </Row>
        {!this.props.teacher
          ?
            <div>
              <RadialProgressChart
                maxValue={this.props.theme.totalNumber}
                currentValue={this.props.theme.numberOfCompleted}
                id={this.props.theme.uuid}
                arc={false}
                color={'#1890ff'}
              />
              <Row type="flex" justify="center">
                <Col>
                <p>{`${this.props.theme.totalNumber} activité(s)`}</p>
                </Col>
              </Row>
            </div>
          :
            <div style={{ marginBottom: 10 }}>
              <Row type="flex" justify="center">
                <Col>
                  <h1 style={{ marginBottom: 0, fontSize: 50 }}>{`${this.props.theme.totalNumber}`}</h1>
                </Col>
              </Row>
              <Row type="flex" justify="center"> 
                <Col>
                  <p>{`activité(s)`}</p>
                </Col>
              </Row>
            </div>
        }
        
        <Row type="flex" justify="center">
          <Col>
            <Row type="flex" justify="center">
              <Button type="primary" onClick={() => this.props.navigate(this.props.theme.uuid)}>Consulter</Button>
            </Row>
          </Col>
        </Row>
      </div>
    );
  }
};

export default ThemeCard;