import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import * as d3 from "d3";

class CardLineChart extends Component {
  constructor(props) {
    super(props);

    this.state = {
      margin: {top: 20, right: 20, bottom: 30, left: 50}
    }
  }

  componentDidMount() {
    const svg = d3.select(`#${this.props.id}`);

    const width = parseInt(svg.style("width")) - this.state.margin.left - this.state.margin.right;
    const height = parseInt(svg.style("height")) - this.state.margin.top - this.state.margin.bottom;

    const parseTime = d3.timeParse("%d-%m-%y");

    const g = svg.append("g")
      .attr("id", `${this.props.id}-g`)
      .attr("transform", "translate(" + this.state.margin.left + "," + this.state.margin.top + ")")
      .attr("width", width);

    const x = d3.scaleTime().rangeRound([0, width]);
    const y = d3.scaleTime().rangeRound([height, 0]);

    const line = d3.line()
      .x((d) => { return x(d.date); })
      .y((d) => { return y(d.data); });

    d3.select(window).on(`resize.${this.props.id}`, this.resize);

    d3.csv(`${this.props.dataPath}`, (d) => {
      d.date = parseTime(d.date);
      d.data = +d.data;
      return d;
    }, (error, data) => {
      if (error) throw error;

      x.domain(d3.extent(data, function(d) { return d.date; }));
      y.domain(d3.extent(data, function(d) { return d.data; }));

      const xAxis = g.append("g")
        .attr("transform", "translate(0," + height + ")")
        .call(d3.axisBottom(x).ticks(4));
      
      xAxis.select('.domain').remove();

      this.setState({
        data: data,
        xAxis: xAxis,
      });

      g.append("g")
        .attr("class", "y axis")
        .call(d3.axisLeft(y))
        .append("text")
        .attr("fill", "#000")
        .attr("transform", "rotate(-90)")
        .attr("dy", "0.71em")
        .attr("text-anchor", "end")
        .text("Price ($)");

      g.append("path")
        .datum(data)
        .attr("class", "line")
        .attr("fill", "none")
        .attr("stroke", "#76bf5a")
        .attr("stroke-linejoin", "round")
        .attr("stroke-linecap", "round")
        .attr("stroke-width", 3)
        .attr("d", line)

      this.resize();
    });
  }

  resize = () => {
    const svg = d3.select(`#${this.props.id}`);

    const margin = {top: 20, right: 20, bottom: 30, left: 50};

    const width = parseInt(svg.style("width")) - this.state.margin.left - this.state.margin.right;
    const height = parseInt(svg.style("height")) - this.state.margin.top - this.state.margin.bottom;

    d3.select(`#${this.props.id}-g`).attr("width", width)

    const x = d3.scaleTime().rangeRound([0, width]);
    const y = d3.scaleTime().rangeRound([height, 0]);

    x.domain(d3.extent(this.state.data, function(d) { return d.date; }));
    y.domain(d3.extent(this.state.data, function(d) { return d.data; }));

    const line = d3.line()
    .x((d) => { return x(d.date); })
    .y((d) => { return y(d.data); });

    const lineSvg = svg.selectAll('.line')
    
    this.state.xAxis.call(d3.axisBottom(x).ticks(4))

    this.state.xAxis.select('.domain').remove();

    lineSvg.attr("d", line);
  }

  render() {
    return (
      <div style={{ marginBottom: 20, backgroundColor: '#fafafa' }}>
        <h1 style={{ textAlign: 'center' }}>{this.props.title}</h1>
        <svg width="100%" height="400" id={this.props.id}></svg>
      </div>
    );
  }
};

export default CardLineChart;
