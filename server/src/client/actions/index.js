export const FETCH_USERS = "fetch_users";
export const fetchUsers = () => async (dispatch, getState, api) => {
  const res = await api.get("/users");
  dispatch({
    type: FETCH_USERS,
    payload: res
  });
};

export const AUTHENTICATE_USER = "authenticate_user";
export const ERROR_AUTHENTICATE_USER = "error_authenticate_user";
export const authenticateUser = values => async (dispatch, getState, api) => {
  try {
    const res = await api.post("/login", values);
    dispatch({
      type: AUTHENTICATE_USER,
      payload: res
    });
  } catch (error) {
    dispatch({
      type: ERROR_AUTHENTICATE_USER
    });
    return error.response.data;
  }
};

export const FETCH_CURRENT_USER = "fetch_current_user";
export const fetchCurrentUser = () => async (dispatch, getState, api) => {
  const res = await api.get("/current_user");

  dispatch({
    type: FETCH_CURRENT_USER,
    payload: res
  });
};

export const FETCH_ADMINS = "fetch_admins";
export const fetchAdmins = () => async (dispatch, getState, api) => {
  const res = await api.get("/admins");

  dispatch({
    type: FETCH_ADMINS,
    payload: res
  });
};

export const FETCH_MY_CLASSES = "fetch_my_classes";
export const fetchMyClasses = () => async (dispatch, getState, api) => {
  const res = await api.get("/myClasses");
  dispatch({
    type: FETCH_MY_CLASSES,
    payload: res
  });
};

export const FETCH_A_CLASSE = "fetch_a_classe";
export const fetchAClasse = id => async (dispatch, getState, api) => {
  const res = await api.get(`/classe/${id}`);
  dispatch({
    type: FETCH_A_CLASSE,
    payload: res
  });
};

export const ADD_THEME_TO_CLASSE = "add_theme_to_classe";
export const addThemeToClasse = (classeUuid, themeUuid) => async (
  dispatch,
  getState,
  api
) => {
  const res = await api.post("/addThemeToClasse/", { classeUuid, themeUuid });
  dispatch({
    type: ADD_THEME_TO_CLASSE,
    payload: { classeUuid, themeUuid }
  });
};

export const REMOVE_THEME_TO_CLASSE = "remove_theme_to_classe";
export const removeThemeToClasse = (classeUuid, themeUuid) => async (
  dispatch,
  getState,
  api
) => {
  const res = await api.post(`/removeThemeToClasse/`, {
    classeUuid,
    themeUuid
  });
  dispatch({
    type: REMOVE_THEME_TO_CLASSE,
    payload: { classeUuid, themeUuid }
  });
};

export const FETCH_THEMES = "fetch_themes";
export const fetchThemes = () => async (dispatch, getState, api) => {
  const res = await api.get("/themes");
  dispatch({
    type: FETCH_THEMES,
    payload: res
  });
};

export const FETCH_CURRENT_THEME = "fetch_current_theme";
export const fetchCurrentTheme = id => async (dispatch, getState, api) => {
  const res = await api.get(`/theme/${id}`);
  dispatch({
    type: FETCH_CURRENT_THEME,
    payload: res
  });
};

export const UPDATE_THEME_TITLE = "upadte_theme_title";
export const updateThemeTitle = (title, id) => async (
  dispatch,
  getState,
  api
) => {
  const res = await api.put("/theme/title", { themeUuid: id, title });
  dispatch({
    type: UPDATE_THEME_TITLE,
    payload: { themeUuid: id, title }
  });
};

export const FETCH_ACTIVITY = "fetch_activity";
export const fetchActivity = id => async (dispatch, getState, api) => {
  const res = await api.get(`/activity/${id}`);
  dispatch({
    type: FETCH_ACTIVITY,
    payload: res
  });
};

export const COMPLETE_ACTIVITY = "complete_activity";
export const completeActivity = id => async (dispatch, getState, api) => {
  dispatch({
    type: COMPLETE_ACTIVITY,
    payload: id
  });
};

export const REMOVE_CURRENT_ACTIVITY = "remove_current_activity";
export const removeCurrentActivity = () => async (dispatch, getState, api) => {
  dispatch({
    type: REMOVE_CURRENT_ACTIVITY
  });
};

export const CREATE_STATION = "create_station";
export const createStation = values => async (dispatch, getState, api) => {
  const res = await api.post(`/station/add`, values);
  dispatch({
    type: CREATE_STATION,
    payload: res
  });
  return res;
};
