import React from 'react';
import { Helmet } from 'react-helmet';

export default () => {
  const helmet = Helmet.renderStatic();

  // Here we send the initial state to the clint by the window.INITIAL_STATE object
  // serialize() is used bellow in order to avoid XSS attacks
  return `
    <html>
      <head>
        ${helmet.title.toString()}
        ${helmet.meta.toString()}
        <link rel="stylesheet" href="/style/style.css">
      </head>
      <body>
        <div id="root"></div>
        <script src="/js/bundle.js"></script>
      </body>
    </html>
  `;
};
