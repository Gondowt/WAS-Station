const path = require('path');

const config = {
  // Tell webpack the root file of our
  // server application
  name: 'client',
  target: 'web',
  entry: './src/client/client.js',

  // Tell webpack where to put the output file
  // that is generated
  output: {
    filename: 'js/bundle.js',
    path: path.resolve(__dirname, 'public')
  },
  module: {
    rules: [
      {
        // Add .jsx here to use jsx files
        test: /\.js?$/,
        loader: 'babel-loader',
        exclude: /node_modules/,
        options: {
          // preset react turn jsx into js
          // stage-0 is useful for async/await
          presets: [
            'react',
            'stage-0',
            ['env', { targets: { browsers: ['last 2 versions'] } }]
          ],
          plugins: [
            ["import", { libraryName: "antd", style: "css" }],
            ["transform-class-properties", { "spec": true }]
          ]
        }
      },
      {
        test: /\.css$/,
        include: [/node_modules\/.*antd/],
        use: [
          {
            loader: 'style-loader',
          },
          {
            loader: 'css-loader',
          },
        ],
      },
    ]
  }
};

module.exports = config;
