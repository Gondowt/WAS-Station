const mysql = require("mysql");

module.exports = {
  connect: () =>
    mysql.createConnection({
      host: "mysql",
      database: "my_db",
      user: "user",
      password: "example",
      multipleStatements: true
    })
};
