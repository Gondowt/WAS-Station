# WAS-Station Project

## Setup project

### Launch docker micro services
```
docker-compose build
docker-compose up
```

## Technologies

- [ReactJS](https://reactjs.org)
- [NodeJS](https://nodejs.org)
- [Neo4J](https://neo4j.com)
- [Docker](http://docker.com)
- [Ant Design](https://ant.design)