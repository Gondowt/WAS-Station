const requireTeacher = require('../middlewares/requireTeacher');
const stationController = require('../controllers/stationController');

module.exports = (app) => {
  app.post('/station/add', requireTeacher, stationController.createNewStation);
  app.post('/station/activate', stationController.activateStation);
};
