const passport = require('passport');

module.exports = (app) => {
  app.post(
    '/login',
    passport.authenticate('local', {
      failWithError: true,
    }),
    (req, res) => res.status(200).send(req.user),
    (err, req, res, next) => res.status(403).send(err),
  );

  app.get('/logout', (req, res) => {
    req.logout();
    res.redirect('/');
  });

  app.get('/current_user', (req, res) => {
    res.send(req.user);
  });
};
