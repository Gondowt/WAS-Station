const requireLogin = require('../middlewares/requireLogin');
const requireTeacher = require('../middlewares/requireTeacher');
const classesController = require('../controllers/classesController');

module.exports = (app) => {
  app.post('/classes', requireTeacher, classesController.createNewClasse);

  app.post('/classe/addUser', requireTeacher, classesController.addUserIntoClasse);

  app.get('/classe/:id', requireLogin, classesController.getAClasse);

  app.get('/myClasses', requireLogin, classesController.getMyClasses);
};
