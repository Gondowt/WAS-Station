#!/usr/bin/env node

const Model = require('../models/index.js');

Model.User.create({ username: 'Teacher', password: 'azerty', role: 'teacher'}, (err, result) => {
    const teacherUuid = result[0]["le.uuid"];
    Model.Classe.create({ name: "4A" }, { uuid: teacherUuid }, (err, result) => {
        const classeUuid = result[0]["b.uuid"];
        Model.User.createStudent({ username: "Student1", password: 'azerty', role: 'student' }, classeUuid, () => {});
        Model.User.createStudent({ username: "Student2", password: 'azerty', role: 'student' }, classeUuid, () => {});
        Model.User.createStudent({ username: "Student3", password: 'azerty', role: 'student' }, classeUuid, () => {});
    });
});

