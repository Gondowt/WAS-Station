const db = require('./db.js');

const Model = {
  User: require('./User.js')(db),
  Classe: require('./Classe.js')(db),
  Theme: require('./Theme.js')(db),
  Activity: require('./Activity.js')(db),
  Station: require('./Station.js')(db),
};

module.exports = Model;
