const uuidv4 = require('uuid/v4');

module.exports = function(db) {
    function query(query, cb) {
        db.cypher({ query: query }, cb);
    }

    return {
      create: function(classe, user, callback) {
        const uuid = uuidv4();
        query(`CREATE (classe:Classe { name: '${classe.name}', uuid: '${uuid}' })`, () => {
          query(`MATCH (a:Person),(b:Classe) WHERE a.uuid = '${user.uuid}' AND b.uuid = '${uuid}'
            CREATE (a)-[r:HAS]->(b) RETURN b.uuid`, callback)
        });
      },
      getUserClasses: function(user, callback) {
        query(`MATCH (:Person { uuid: '${user.uuid}'})-[:HAS]->(classes)
          RETURN classes`, callback)
      },
      getAClasse: function(id, callback) {
        query(`MATCH (c:Classe)
          WHERE c.uuid = '${id}'
          OPTIONAL MATCH (c)<-[:ISIN]-(p:Person) RETURN c AS classe, collect(p) as students;`, (err, result) => {
          return callback(null, result);
        })
      },
      addUserIntoClasse: function(classeId, username, callback) {
        query(`MATCH (a:Person),(b:Classe) WHERE a.name = '${username}' AND b.uuid = '${classeId}'
            CREATE (a)-[r:ISIN]->(b) RETURN r`, callback)
      }
    };
  };
