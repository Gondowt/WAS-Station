const uuidv4 = require('uuid/v4');

module.exports = function(db) {
    function query(query, cb) {
        db.cypher({ query: query }, cb);
    }

    return {
      create: function(activity, callback) {
        const uuid = uuidv4();
        query(`CREATE (ac:Activity { name: '${activity.name}', uuid: '${uuid}', type: '${activity.type}' })`, callback);
      },
      updateDataActivity: function(activityUuid, data, soluce, callback) {
        query(`MATCH (ac:Activity {uuid: '${activityUuid}'}) SET ac.data = '${data}', ac.soluce = '${soluce}' RETURN ac`, callback);   
      },
      addIntoTheme: function(themeUuid, activityUuid, callback) {
        query(`MATCH (a:Theme),(b:Activity) WHERE a.uuid = '${themeUuid}' AND b.uuid = '${activityUuid}'
                    CREATE (a)<-[r:ISINTO]-(b) RETURN r`, callback)
      },
      findLastActivity: function(themeUuid, callback) {
        query(`MATCH (:Theme { uuid: '${themeUuid}' })<-[:ISINTO]-(n:Activity) WHERE NOT (n:Activity)-[:HASNEXT]->(:Activity) RETURN n`, callback)
      },
      getNextActivityUuid: function(activityUuid, callback) {
        query(`MATCH (:Activity {uuid: '${activityUuid}'})-[:HASNEXT]->(n:Activity) RETURN n.uuid`, callback)
      },
      getActivitySoluce: function(activityUuid, callback) {
        query(`MATCH (ac:Activity {uuid: '${activityUuid}'}) RETURN ac.soluce`, callback)
      },
      getActivity: function(activityUuid, callback) {
        query(`MATCH (n:Activity {uuid: '${activityUuid}'}) RETURN n`, callback)
      },
      completeActivity: function(activityUuid, user, correct, total, callback) {
        query(`MATCH (a:Activity {uuid: '${activityUuid}'}), (b:Person {uuid: '${user.uuid}'}) CREATE (a)<-[:DONE { correct: '${correct}', total: '${total}'}]-(b)`, callback)
      }
    };
  };
