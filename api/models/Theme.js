const uuidv4 = require('uuid/v4');

module.exports = function(db) {
    function query(query, cb) {
        db.cypher({ query: query }, cb);
    }

    return {
      create: function(theme, user, callback) {
        const uuid = uuidv4();
        query(`CREATE (theme:Theme { name: '${theme.name}', uuid: '${uuid}' })`, () => {
          query(`MATCH (a:Theme { uuid: '${uuid}'}), (b:Person { uuid: '${user.uuid}'}) CREATE (a)<-[r:HASCREATED]-(b) return r`, callback)
        });
      },
      addThemeToClasse: function(themeUuid, classeUuid, callback) {
        query(`MATCH (a:Theme),(b:Classe) WHERE a.uuid = '${themeUuid}' AND b.uuid = '${classeUuid}'
                    CREATE (a)<-[r:HAS]-(b) RETURN r`, callback)
      },
      removeThemeToClasse: function(themeUuid, classeUuid, callback) {
        query(`MATCH (a:Theme { uuid: '${themeUuid}' })<-[r:HAS]-(b:Classe { uuid: '${classeUuid}'})
                    DELETE r`, callback)
      },
      getTheme: function(themeUuid, callback) {
        query(`MATCH (n:Theme { uuid: '${themeUuid}'}) RETURN n`, callback);
      },
      getThemeTeacher: function(themeUuid, callback) {
        query(`MATCH (n:Theme { uuid: '${themeUuid}'}) OPTIONAL MATCH (n)<-[:HAS]-(c:Classe) RETURN n, collect(c) AS listOfClasses`, callback);
      },
      getThemeActivities: function(themeUuid, callback) {
        query(`MATCH (a:Activity)-[:ISINTO]->(:Theme { uuid: '${themeUuid}'})
          WHERE NOT ()-[:HASNEXT]->(a:Activity)
          OPTIONAL MATCH (a)-[:HASNEXT*]->(b:Activity)
          RETURN a + collect(b) AS a`, callback)
      },
      getThemeActivitiesStudent: function(themeUuid, user, callback) {
        query(`MATCH (a:Activity)-[:ISINTO]->(:Theme { uuid: '${themeUuid}'})
          WHERE NOT ()-[:HASNEXT]->(a:Activity)
          OPTIONAL MATCH (a)-[:HASNEXT*]->(b:Activity)
          RETURN a + collect(b) AS listOfActivities, exists((a)<-[:DONE]-(:Person { uuid: '${user.uuid}' })) + collect(exists((b)<-[:DONE]-(:Person { uuid: '${user.uuid}' }))) AS isDone`, callback)
      },
      getTeacherThemes: function(userUuid, callback) {
        query(`MATCH (n:Theme)
          WHERE (:Person {uuid: '${userUuid}'})-[:HAS]->(:Classe)-[:HAS]->(n)
          OR (:Person {uuid: '${userUuid}'})-[:HASCREATED]->(n)
          OPTIONAL MATCH (n)<-[:ISINTO]-(a:Activity)
          RETURN n, count(a) AS totalNumber`, callback)
      },
      getMyClasseThemes: function(userUuid, callback) {
        query(`MATCH (p:Person { uuid: '${userUuid}'})-[:ISIN]->(:Classe)-[:HAS]->(t:Theme)
          OPTIONAL MATCH (t)<-[:ISINTO]-(a:Activity)
          OPTIONAL MATCH (t)<-[:ISINTO]-(b:Activity)<-[:DONE]-(p)
          RETURN { theme: t, totalNumber: count(distinct a), numberOfCompleted: count(distinct b)} AS theme`, callback)
      },
      updateThemeTitle: function(themeUuid, title, callback) {
        query(`MATCH (t:Theme { uuid: '${themeUuid}' }) SET t.name = '${title}'`, callback);
      }
    };
  };
