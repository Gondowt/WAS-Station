const bcrypt = require('bcryptjs');
const keys = require('../config/keys');
const uuidv4 = require('uuid/v4');

module.exports = (db) => {
  function query(query, cb) {
    db.cypher({ query }, cb);
  }

  return {
    create(user, callback) {
      query(
        `MATCH (us:Person) WHERE us.name = '${user.username}' RETURN us as Person;`,
        (err, results) => {
          if (results === null) {
            if (results.length > 0) {
              return callback('Username already exists');
            }
          }
          bcrypt.genSalt(process.env.BCRYPT_SALT, (err, salt) => {
            if (err) return callback(err);
            bcrypt.hash(user.password, salt, (err, hash) => {
              if (err) return callback(err);
              user.password = hash;
              const uuid = uuidv4();
              query(
                `CREATE (le:Person { name: '${user.username}', password: '${
                  user.password
                }', role: '${user.role}', uuid: '${uuid}' }) RETURN le.uuid`,
                callback,
              );
            });
          });
        },
      );
    },
    createStudent(user, classeUuid, callback) {
      query(
        `MATCH (us:Person) WHERE us.name = '${user.username}' RETURN us as Person;`,
        (err, results) => {
          if (results.length > 0) {
            return callback('Username already exists');
          }
          bcrypt.genSalt(keys.bcrypt.salt, (err, salt) => {
            if (err) return callback(err);
            bcrypt.hash(user.password, salt, (err, hash) => {
              if (err) return callback(err);
              user.password = hash;
              const uuid = uuidv4();
              query(
                `CREATE (le:Person { name: '${user.username}', password: '${
                  user.password
                }', role: '${user.role}', uuid: '${uuid}' })`,
                () => {
                  query(
                    `MATCH (a:Person),(b:Classe) WHERE a.uuid = '${uuid}' AND b.uuid = '${classeUuid}'
                    CREATE (a)-[r:ISIN]->(b) RETURN r`,
                    callback,
                  );
                },
              );
            });
          });
        },
      );
    },
    update(uuid, user, callback) {
      query(`MATCH (n { uuid: '${uuid}' }) SET n.role = '${user.role}'  RETURN n;`, callback);
    },
    getByUsername(username, callback) {
      query(`MATCH (us:Person) WHERE us.name = '${username}' RETURN us as Person;`, callback);
    },
    getByUuid(uuid, callback) {
      query(`MATCH (us:Person) WHERE us.uuid = '${uuid}' RETURN us as Person;`, callback);
    },
    delete(uuid, callback) {
      query(`MATCH (us:Person) WHERE us.uuid = '${uuid}' DETACH DELETE us;`, callback);
    },
    validPassword(password, hash, callback) {
      bcrypt.compare(password, hash, callback);
    },
  };
};
