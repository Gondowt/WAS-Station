const uuidv4 = require('uuid/v4');

module.exports = (db) => {
  function query(query, cb) {
    db.cypher({ query }, cb);
  }

  return {
    create(station, callback) {
      const uuid = uuidv4();
      query(
        `CREATE (st:Station
        { name: '${station.name}', uuid: '${uuid}', place: '${station.place}' })`,
        (err, results) => callback(err, uuid),
      );
    },
    activate(station, callback) {
      query(
        `MATCH (st:Station
        { uuid: '${station.uuid}' }) SET st.activated = true RETURN st`,
        callback,
      );
    },
  };
};
