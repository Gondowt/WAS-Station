const data = {
  img: "/img/Activity/UV/Activity2/background.png",
  height: 583,
  width: 1800,
  dropzones : [
    {
      x: 590,
      y: 79,
      width: 303,
      height: 116,
    },
    {
      x: 904,
      y: 79,
      width: 312,
      height: 116,
    },
    {
      x: 1226,
      y: 79,
      width: 292,
      height: 116,
    },
    {
      x: 904,
      y: 206,
      width: 312,
      height: 116,
    },
    {
      x: 1226,
      y: 206,
      width: 292,
      height: 116,
    },
    {
      x: 590,
      y: 333,
      width: 303,
      height: 116,
    },
    {
      x: 904,
      y: 333,
      width: 312,
      height: 116,
    },
    {
      x: 1226,
      y: 333,
      width: 292,
      height: 116,
    },
    {
      x: 590,
      y: 460,
      width: 303,
      height: 116,
    },
    {
      x: 1226,
      y: 460,
      width: 292,
      height: 116,
    }
  ],
  dragElements : [
    {
      width: 295,
      height: 116,
      img: "/img/Activity/UV/Activity2/de1.png"
    },
    {
      width: 305,
      height: 116,
      img: "/img/Activity/UV/Activity2/de2.png"
    },
    {
      width: 285,
      height: 116,
      img: "/img/Activity/UV/Activity2/de3.png"
    },
    {
      width: 312,
      height: 116,
      img: "/img/Activity/UV/Activity2/de4.png"
    },
    {
      width: 287,
      height: 116,
      img: "/img/Activity/UV/Activity2/de5.png"
    },
    {
      width: 293,
      height: 116,
      img: "/img/Activity/UV/Activity2/de6.png"
    },
    {
      width: 302,
      height: 116,
      img: "/img/Activity/UV/Activity2/de7.png"
    },
    {
      width: 282,
      height: 116,
      img: "/img/Activity/UV/Activity2/de8.png"
    },
    {
      width: 292,
      height: 116,
      img: "/img/Activity/UV/Activity2/de9.png"
    },
    {
      width: 282,
      height: 116,
      img: "/img/Activity/UV/Activity2/de10.png"
    }
  ]
}
  
  module.exports = data;
  