const Model = require('../models/index.js');
const uuidv4 = require('uuid/v4');
const omit = require('object.omit');

const ActivitiesController = {
  createNewActivity(req, res) {
    const { name, type } = req.body;
    Model.Activity.create({ name, type }, (err) => {
      if (err) return console.error(err);
      res.status(200).send('activity created');
    });
  },
  addIntoTheme(req, res) {
    const { themeUuid, activityUuid } = req.body;
    Model.Activity.addIntoTheme(themeUuid, activityUuid, (err) => {
      if (err) return console.error(err);
      res.status(200).send('activity added into theme');
    });
  },
  getActivity(req, res) {
    Model.Activity.getActivity(req.params.id, (err, result) => {
      if (err) return console.error(err);
      if (!result || result.length < 1) return res.status(200).send({});
      if (!result[0].n.properties.data) return res.status(200).send({});
      let omited = omit(result[0].n.properties, 'soluce');
      omited = {
        ...omited,
        data: JSON.parse(omited.data),
      };
      Model.Activity.getNextActivityUuid(req.params.id, (err, result) => {
        if (err) return console.error(err);
        res.status(200).send({
          ...omited,
          nextActivity: result.length > 0 ? result[0]['n.uuid'] : null,
        });
      });
    });
  },
  checkActivity(req, res) {
    if (!req.params.id) {
      return res.status(422).send('You need to send an activity id');
    }
    Model.Activity.getActivitySoluce(req.params.id, (err, soluces) => {
      if (!req.user) {
        return res.status(403).send('You need to be authenticate');
      }
      if (soluces && soluces.length > 0) {
        const solucesParsed = JSON.parse(soluces[0]['ac.soluce']);
        let isCompleted = true;
        let correctAnswer = 0;
        let results = [];
        switch (req.body.type) {
          case 'dragDrop':
            results = solucesParsed.map((item, index) => {
              if (
                item.idDrop === req.body.data[index].idDrop &&
                item.idDrag === req.body.data[index].idDrag
              ) {
                correctAnswer += 1;
                return {
                  idDropZone: item.idDrop,
                  result: true,
                };
              }
              isCompleted = false;
              return {
                idDropZone: item.idDrop,
                result: false,
              };
            });
            break;
          case 'trucARelier':
            results = solucesParsed.map((item, index) => {
              if (req.body.data[item.idStartZone] === item.idEndZone) {
                correctAnswer += 1;
                return {
                  idStartZone: item.idStartZone,
                  idEndZone: item.idEndZone,
                  result: true,
                };
              }
              isCompleted = false;
              return {
                idStartZone: item.idStartZone,
                idEndZone: item.idEndZone,
                result: false,
              };
            });
            break;
          case 'quizz':
            results = solucesParsed.map((item) => {
              let answerQuestion = true;
              return {
                id: `q${item.question}`,
                result: item.reponse.map((answer) => {
                  answerQuestion =
                    answerQuestion &&
                    answer.answer ===
                      (req.body.data[`q${item.question}`].indexOf(answer.id) !== -1);
                  return {
                    id: answer.id,
                    result:
                      answer.answer ===
                      (req.body.data[`q${item.question}`].indexOf(answer.id) !== -1),
                  };
                }),
                answer: answerQuestion,
              };
            });
            results.map((item) => {
              correctAnswer += item.answer ? 1 : 0;
            });
            break;
          default:
            break;
        }
        Model.Activity.completeActivity(
          req.params.id,
          req.user,
          correctAnswer,
          soluces.length,
          () =>
            res.status(200).send({
              isCompleted,
              results,
            }),
        );
      } else {
        return res.status(204).send('Soluce cannot be found');
      }
    });
  },
  setActivityData(req, res) {
    let data;
    let soluce;
    switch (req.body.type) {
      case 'dragDrop':
        data = Object.assign({}, req.body.data);
        soluce = [];
        data = {
          ...data,
          dropzones: data.dropzones.map((item, index) => {
            const dropzoneUuid = uuidv4();
            const dragElementUuid = uuidv4();
            soluce.push({
              idDrop: dropzoneUuid,
              idDrag: dragElementUuid,
            });
            data.dragElements[index] = {
              ...data.dragElements[index],
              id: dragElementUuid,
            };
            return {
              ...item,
              id: dropzoneUuid,
            };
          }),
        };
        break;
      case 'trucARelier':
        data = Object.assign({}, req.body.data);
        soluce = [];
        data = {
          ...data,
          startZones: data.startZones.map((item, index) => {
            const startZoneUuid = uuidv4();
            const endZoneUuid = uuidv4();
            soluce.push({
              idStartZone: startZoneUuid,
              idEndZone: endZoneUuid,
            });
            data.endZones[index] = {
              ...data.endZones[index],
              id: endZoneUuid,
            };
            return {
              ...item,
              id: startZoneUuid,
            };
          }),
        };
        break;
      case 'quizz':
        data = req.body.data.slice();
        soluce = [];
        data = data.map((item) => {
          const questionUuid = uuidv4();

          const reponse = item.reponse.map((item_reponse, index) => {
            const reponseUuid = uuidv4();

            return {
              ...item_reponse,
              id: reponseUuid,
            };
          });

          soluce.push({
            question: questionUuid,
            reponse: reponse.map((item_reponse, index) => {
              const { answer } = reponse[index];
              delete reponse[index].answer;
              return {
                id: item_reponse.id,
                answer,
              };
            }),
          });

          return {
            ...item,
            id: questionUuid,
            reponse,
          };
        });
        break;
      default:
        break;
    }
    Model.Activity.updateDataActivity(
      req.params.id,
      JSON.stringify(data).replace(/'/g, "\\'"),
      JSON.stringify(soluce).replace(/'/g, "\\'"),
      (err) => {
        if (err) return console.error(err);
        res.status(200).send('done');
      },
    );
  },
};

module.exports = ActivitiesController;
