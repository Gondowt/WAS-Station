const Model = require('../models/index.js');
const axios = require('axios');

const ActivitiesController = {
  createNewStation(req, res) {
    const { name, place } = req.body;
    Model.Station.create({ name, place }, (err, uuid) => {
      if (err) return console.error(err);
      axios
        .post('http://dataserver:9000/station/create', { uuid })
        .then((response) => {
          if (response.status === 200) {
            return res.status(200).send(response.data);
          }
          return res.status(500).send('Error creating station');
        })
        .catch((error) => {
          console.log(error);
        });
    });
  },
  activateStation(req, res) {
    const { uuid } = req.body;
    Model.Station.activate({ uuid }, (err, results) => {
      if (err) {
        res.status(500).send('Internal error');
        return console.error(err);
      }
      if (results.length < 1) {
        return res.status(500).send('Cannot find this station');
      }
      res.status(200).send('station activated');
    });
  },
};

module.exports = ActivitiesController;
