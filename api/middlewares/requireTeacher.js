module.exports = (req, res, next) => {
  if (!req.user || req.user.role !== 'teacher') {
    return res.status(401).send({ error: 'You must a Teacher !' });
  }
  next();
};
